# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.5](https://github.com/unleashit/npm-library/compare/@unleashit/modal@0.2.4...@unleashit/modal@0.2.5) (2022-02-17)

**Note:** Version bump only for package @unleashit/modal





## [0.2.4](https://github.com/unleashit/npm-library/compare/@unleashit/modal@0.2.3...@unleashit/modal@0.2.4) (2022-01-22)

**Note:** Version bump only for package @unleashit/modal





## [0.2.3](https://github.com/unleashit/npm-library/compare/@unleashit/modal@0.2.2...@unleashit/modal@0.2.3) (2022-01-22)

**Note:** Version bump only for package @unleashit/modal





## [0.2.2](https://github.com/unleashit/npm-library/compare/@unleashit/modal@0.2.1...@unleashit/modal@0.2.2) (2021-08-16)

**Note:** Version bump only for package @unleashit/modal





## [0.2.1](https://github.com/unleashit/npm-library/compare/@unleashit/modal@0.2.0...@unleashit/modal@0.2.1) (2021-08-16)

**Note:** Version bump only for package @unleashit/modal





# [0.2.0](https://github.com/unleashit/npm-library/compare/@unleashit/modal@0.1.0...@unleashit/modal@0.2.0) (2020-06-30)


### Features

* **modal:** support escape key, fix z-index issue ([dbad01e](https://github.com/unleashit/npm-library/commit/dbad01e1905d1e68c5f946975c8492704efc8b47))





# 0.1.0 (2020-05-21)


### Features

* **modal:** WIP ([6f5af82](https://github.com/unleashit/npm-library/commit/6f5af82))
* **modal:** wip add modal component ([3af40ab](https://github.com/unleashit/npm-library/commit/3af40ab))
